const express = require('express');
const axios = require('axios');
const multer = require('multer');
const FormData = require('form-data');
const cors = require('cors');
const app = express();
require('dotenv').config();
const port = process.env.PORT||3002;
app.use(cors());
// Configure Multer for handling file uploads
const storage = multer.memoryStorage();
const upload = multer({ storage: storage });

app.use(express.json());

// Define a route for handling image uploads
app.post('/upload', upload.single('image'), async (req, res) => {
  try {
    // Set the headers for the external API request
    const headers = {
      'Content-Type': 'multipart/form-data',
      'promiseq-subscription-key': '77158c11-2489-41ed-a973-1e',
      'client-id': 'default-client',
    };
    console.log("func call")

    // Create form data
    const formData = new FormData();
    formData.append('image', req.file.buffer, { filename: req.file.originalname });

    // Make the external API request
    const externalApiResponse = await axios.post('https://europe-west1-promiseq-production2.cloudfunctions.net/sendToThreatDetect', formData, { headers });

    // Send the external API response back to the client
    res.json(externalApiResponse.data);
    console.log(externalApiResponse.data)
  } catch (error) {
    // Handle errors
    console.error('Error handling image upload:', error.message);
    res.status(500).json({ error: 'Internal Server Error' });
  }
});

// Start the Express server
app.listen(port, () => {
  console.log(`Server is running at http://localhost:${port}`);
});
